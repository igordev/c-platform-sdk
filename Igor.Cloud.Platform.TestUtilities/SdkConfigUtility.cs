﻿using System.Collections.Generic;
using System.Linq;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Igor.Cloud.Platform.TestUtilities
{
    public static class SdkConfigUtility
    {
        public static IConfiguration GetConfiguration(ApiSettings apiSettings = null)
        {
            var settings = apiSettings ?? new ApiSettings();
            var configValues = settings.GetType().GetProperties().ToDictionary(
                propertyInfo => $"ApiSettings:{propertyInfo.Name}",
                propertyInfo => propertyInfo.GetValue(settings).ToString());

            var config = new ConfigurationBuilder()
                .AddInMemoryCollection(configValues)
                .Build();
            
            return config;
        }
    }
}