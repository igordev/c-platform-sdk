IF /I "%APPVEYOR_REPO_BRANCH%"=="master" (
    call dotnet pack
) ELSE (
    call dotnet pack --version-suffix "prerelease%APPVEYOR_BUILD_NUMBER%"
)