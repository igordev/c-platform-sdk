﻿namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers
{
    public class CreateCustomerDto
    {
        public string Name { get; set; }
    }
}
