﻿using System;
using System.Collections.Generic;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Users;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers
{
    public class CustomerDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public IList<UserDto> Users { get; set; }

        public IList<CustomerDto> Customers { get; set; }

        public bool IsPrimary { get; set; }
    }
}
