﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Sdk.Common.Models;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers
{
    public class CustomerService : ServiceBase, ICustomerService
    {
        private const string CustomersRoot = "/customers";

        private readonly string _customersEndpoint;

        public CustomerService(
            IHttpClientService httpClient,
            IJsonConverter jsonConverter,
            ISdkConfig config
            ) 
            : base(httpClient, jsonConverter, config)
        {
            _customersEndpoint = $"{Config.AccountsApiEndpoint}{CustomersRoot}";
        }

        public async Task<CustomerDto> GetAsync(Guid customerId)
        {
            var customerEndpoint = $"{_customersEndpoint}/{customerId}";

            var customer = await GetAsync<CustomerDto>(customerEndpoint);

            return customer;
        }

        public async Task<IPaginatedList<CustomerDto>> GetListAsync(int page = 1, int pageSize = 20)
        {
            var customerEndpoint = $"{_customersEndpoint}?page={page}&pageSize={pageSize}";

            var customers = await GetAsync<PaginatedList<CustomerDto>>(customerEndpoint);

            return customers;
        }

        public async Task<HttpResponseMessage> CreateAsync(CreateCustomerDto customer)
        {
            var serializedCustomer = JsonConverter.Serialize(customer);

            var response = await Post(_customersEndpoint, serializedCustomer);

            return response;
        }

        public async Task<HttpResponseMessage> UpdateAsync(Guid customerId, UpdateCustomerDto customer)
        {
            var customerEndpoint = $"{_customersEndpoint}/{customerId}";

            var serializedCustomer = JsonConverter.Serialize(customer);
            
            var response = await Put(customerEndpoint, serializedCustomer);

            return response;
        }

        public async Task<HttpResponseMessage> DeleteAsync(Guid customerId)
        {
            var customerEndpoint = $"{_customersEndpoint}/{customerId}";

            var response = await Delete(customerEndpoint);

            return response;
        }

        public async Task<HttpResponseMessage> AddUserAsync(Guid customerId, Guid userId)
        {
            var response = await Post(_customersEndpoint + $"/{customerId}/users/{userId}");

            return response;
        }

        public async Task<HttpResponseMessage> RemoveUserAsync(Guid customerId, Guid userId)
        {
            var response = await Delete(_customersEndpoint + $"/{customerId}/users/{userId}");

            return response;
        }

        public async Task<HttpResponseMessage> AddChildCustomerAsync(Guid customerId, Guid childId)
        {
            var response = await Post(_customersEndpoint + $"/{customerId}/customers/{childId}");

            return response;
        }

        public async Task<HttpResponseMessage> RemoveChildCustomerAsync(Guid customerId, Guid childId)
        {
            var response = await Delete(_customersEndpoint + $"/{customerId}/customers/{childId}");

            return response;
        }

        public async Task<IPaginatedList<OperationDto>> GetOperationsForResourceAsync(Guid customerId, int page = 1, int pageSize = 20)
        {
            var customerEndpoint = $"{_customersEndpoint}/{customerId}/operations?page={page}&pageSize={pageSize}";

            var operations = await GetAsync<PaginatedList<OperationDto>>(customerEndpoint);

            return operations;
        }
    }
}
