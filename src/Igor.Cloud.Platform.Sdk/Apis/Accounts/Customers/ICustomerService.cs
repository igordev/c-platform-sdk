﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Sdk.Common.Models;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers
{
    public interface ICustomerService : IHasHttpClient
    {
        Task<IPaginatedList<CustomerDto>> GetListAsync(int page = 1, int pageSize = 20);
        Task<CustomerDto> GetAsync(Guid customerId);

        Task<HttpResponseMessage> CreateAsync(CreateCustomerDto customer);
        Task<HttpResponseMessage> UpdateAsync(Guid customerId, UpdateCustomerDto customer);
        Task<HttpResponseMessage> DeleteAsync(Guid customerId);

        Task<HttpResponseMessage> AddUserAsync(Guid customerId, Guid userId);
        Task<HttpResponseMessage> RemoveUserAsync(Guid customerId, Guid userId);

        Task<HttpResponseMessage> AddChildCustomerAsync(Guid customerId, Guid childId);
        Task<HttpResponseMessage> RemoveChildCustomerAsync(Guid customerId, Guid childId);

        Task<IPaginatedList<OperationDto>> GetOperationsForResourceAsync(Guid userId, int page = 1, int pageSize = 20);
    }
}