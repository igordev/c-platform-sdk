﻿namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers
{
    public class UpdateCustomerDto
    {
        public string Name { get; set; }
    }
}
