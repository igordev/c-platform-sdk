﻿using System;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Users;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Security
{
    public class OperationDto
    {
        public int Id { get; set; }

        public UserDto User { get; set; }

        public OperationType OperationType { get; set; }

        public Guid ResourceId { get; set; }

        public ResourceType ResourceType { get; set; }
    }
}
