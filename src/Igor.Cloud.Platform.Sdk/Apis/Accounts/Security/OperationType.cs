﻿namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Security
{
    public enum OperationType
    {
        None = 0,
        Read = 1,
        Write = 2,
        Delete = 4
    }
}
