﻿namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Security
{
    public enum ResourceType
    {
        User,
        Customer
    }
}
