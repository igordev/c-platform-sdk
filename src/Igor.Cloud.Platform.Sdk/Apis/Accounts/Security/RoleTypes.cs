﻿namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Security
{
    public enum RoleTypes
    {
        IgorAdmin = 1,
        IgorSupport = 2,
        IgorSales = 3,
        DistributorAdmin = 4,
        DisributorUser = 5,
        ResellerAdmin = 6,
        ResellerUser = 7,
        BuildingOwner = 8,
        BuildingTenant = 9,
        ReportViewer = 10,
        IgorService = 11
    }
}
