﻿using System;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Security
{
    public class UpdateOperationDto
    {
        public OperationType OperationType { get; set; }

        public Guid ResourceId { get; set; }

        public ResourceType ResourceType { get; set; }
    }
}
