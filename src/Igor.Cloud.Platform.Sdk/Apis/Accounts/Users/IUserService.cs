﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Sdk.Common.Models;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Users
{
    public interface IUserService : IHasHttpClient
    {
        Task<IPaginatedList<UserDto>> GetListAsync(int page = 1, int pageSize = 20);
        Task<UserDto> GetAsync(Guid userId);

        Task<HttpResponseMessage> CreateAsync(CreateUserDto user);
        Task<HttpResponseMessage> UpdateAsync(Guid userId, UpdateUserDto user);
        Task<HttpResponseMessage> DeleteAsync(Guid userId);

        Task<OperationDto> GetOperationAsync(Guid userId, int operationId);
        Task<HttpResponseMessage> CreateOperationAsync(Guid userId, CreateOperationDto operation);
        Task<HttpResponseMessage> UpdateOperationAsync(Guid userId, int operationId, UpdateOperationDto operation);
        Task<HttpResponseMessage> DeleteOperationAsync(Guid userId, int operationId);

        Task<IPaginatedList<OperationDto>> GetOperationsForResourceAsync(Guid userId, int page = 1, int pageSize = 20);

        Task<HttpResponseMessage> AddUserRoleAsync(Guid userId, RoleTypes role);
        Task<HttpResponseMessage> RemoveUserRoleAsync(Guid userId, RoleTypes role);
    }
}