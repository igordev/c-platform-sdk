﻿using System;
using System.Collections.Generic;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Users
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public bool IsActive { get; set; }
        public string PhoneNumber { get; set; }

        public string Password { get; set; }

        public IList<CustomerDto> Customers { get; set; }

        public IList<RoleTypes> Roles { get; set; }

        public IList<OperationDto> AllowedOperations { get; set; }
    }
}
