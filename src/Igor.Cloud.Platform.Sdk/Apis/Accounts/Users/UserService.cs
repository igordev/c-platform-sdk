﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Sdk.Common.Models;

namespace Igor.Cloud.Platform.Sdk.Apis.Accounts.Users
{
    public class UserService : ServiceBase, IUserService
    {
        private const string UsersRoot = "/users";
        private const string OperationsRoot = "/operations";
        private const string RolesRoot = "/roles";

        private readonly string _usersEndpoint;

        public UserService(
            IHttpClientService httpClient,
            IJsonConverter jsonConverter,
            ISdkConfig config
        )
            : base(httpClient, jsonConverter, config)
        {
            _usersEndpoint = $"{Config.AccountsApiEndpoint}{UsersRoot}";
        }

        public async Task<UserDto> GetAsync(Guid userId)
        {
            var userEndpoint = _usersEndpoint + $"/{userId}";

            var user = await GetAsync<UserDto>(userEndpoint);

            return user;
        }

        public async Task<IPaginatedList<UserDto>> GetListAsync(int page = 1, int pageSize = 20)
        {
            var userEndpoint = _usersEndpoint + $"?page={page}&pageSize={pageSize}";

            var users = await GetAsync<PaginatedList<UserDto>>(userEndpoint);

            return users;
        }

        public async Task<HttpResponseMessage> CreateAsync(CreateUserDto user)
        {
            var serializedUser = JsonConverter.Serialize(user);

            var response = await Post(_usersEndpoint, serializedUser);

            return response;
        }

        public async Task<HttpResponseMessage> UpdateAsync(Guid userId, UpdateUserDto user)
        {
            var userEndpoint = _usersEndpoint + $"/{userId}";

            var serializedUser = JsonConverter.Serialize(user);

            var response = await Put(userEndpoint, serializedUser);

            return response;
        }

        public async Task<HttpResponseMessage> DeleteAsync(Guid userId)
        {
            var userEndpoint = _usersEndpoint + $"/{userId}";

            var response = await Delete(userEndpoint);

            return response;
        }

        public async Task<OperationDto>  GetOperationAsync(Guid userId, int operationId)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}{OperationsRoot}/{operationId}";

            var operation = await GetAsync<OperationDto>(userEndpoint);

            return operation;
        }

        public async Task<IPaginatedList<OperationDto>> GetOperationsForResourceAsync(Guid userId, int page = 1, int pageSize = 20)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}/operations?page={page}&pageSize={pageSize}";

            var operations = await GetAsync<PaginatedList<OperationDto>>(userEndpoint);

            return operations;
        }

        public async Task<HttpResponseMessage> CreateOperationAsync(Guid userId, CreateOperationDto operation)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}{OperationsRoot}";

            var serializedOperation = JsonConverter.Serialize(operation);

            var response = await Post(userEndpoint, serializedOperation);

            return response;
        }

        public async Task<HttpResponseMessage> UpdateOperationAsync(Guid userId, int operationId, UpdateOperationDto operation)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}{OperationsRoot}/{operationId}";

            var serializedOperation = JsonConverter.Serialize(operation);

            var response = await Put(userEndpoint, serializedOperation);

            return response;
        }

        public async Task<HttpResponseMessage> DeleteOperationAsync(Guid userId, int operationId)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}{OperationsRoot}/{operationId}";

            var response = await Delete(userEndpoint);

            return response;
        }

        public async Task<HttpResponseMessage> AddUserRoleAsync(Guid userId, RoleTypes role)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}{RolesRoot}";

            var serializedRole = JsonConverter.Serialize(role.ToString());

            var response = await Post(userEndpoint, serializedRole);

            return response;
        }

        public async Task<HttpResponseMessage> RemoveUserRoleAsync(Guid userId, RoleTypes role)
        {
            var userEndpoint = $"{_usersEndpoint}/{userId}{RolesRoot}/{role}";

            var response = await Delete(userEndpoint);

            return response;
        }
    }
}
