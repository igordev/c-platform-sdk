﻿namespace Igor.Cloud.Platform.Sdk.Apis.Branding.Models
{
    public class BrandingColorDto
    {
        public string DisplayName { get; set; }
        public string VariableName { get; set; }
    }
}