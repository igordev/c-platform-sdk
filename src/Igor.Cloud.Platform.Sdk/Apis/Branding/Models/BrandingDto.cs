﻿namespace Igor.Cloud.Platform.Sdk.Apis.Branding.Models
{
    public class BrandingDto
    {
        public string SchemaVersion { get; set; }
        public string StyleTechnology { get; set; }
        public BrandingColorDto[] DisplayColors { get; set; }
        public string VariablesContent { get; set; }
    }
}