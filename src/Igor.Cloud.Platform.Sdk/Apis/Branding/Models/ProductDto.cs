﻿namespace Igor.Cloud.Platform.Sdk.Apis.Branding.Models
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}