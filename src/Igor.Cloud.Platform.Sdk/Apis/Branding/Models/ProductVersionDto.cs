﻿namespace Igor.Cloud.Platform.Sdk.Apis.Branding.Models
{
    public class ProductVersionDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string PackageName { get; set; }
        public string PackageSource { get; set; }
        public string StartVersion { get; set; }
        public string EndVersion { get; set; }
    }
}