﻿namespace Igor.Cloud.Platform.Sdk.Apis.Branding.Models
{
    public class ProductVersionProfileDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int VersionId { get; set; }

        public bool IsDefault { get; set; }
        public string Name { get; set; }

        public BrandingDto Branding { get; set; }
    }
}