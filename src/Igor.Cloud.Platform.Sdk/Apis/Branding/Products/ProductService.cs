﻿using System.IO;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Branding.Models;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Sdk.Common.Models;

namespace Igor.Cloud.Platform.Sdk.Apis.Branding.Products
{
    public interface IProductService : IHasHttpClient
    {
        Task<IResultList<ProductDto>> GetProducts();
        Task<ProductDto> GetProductById(int id);
        Task<ProductDto> GetProductByName(string productName);
        Task<IResultList<ProductVersionDto>> GetProductVersions(int productId);
        Task<ProductVersionDto> GetProductVersionById(int productId, int versionId);
        Task<ProductVersionDto> GetProductVersionByVersion(int productId, string version);
        Task<IResultList<ProductVersionProfileDto>> GetProductVersionProfiles(int productId, int versionId);

        Task<ProductVersionProfileDto> GetProductVersionProfile(
            int productId, 
            int versionId, 
            int profileId, 
            bool includeBranding = false);

        Task<Stream> GetProductVersionProfileZip(int productId, int versionId, int profileId);
    }

    public class ProductService : ServiceBase, IProductService
    {
        public ProductService(ISdkConfig config)
            : this(new HttpClientService(), new JsonConverter(), config)
        {
        }

        public ProductService(IHttpClientService httpClientService, IJsonConverter jsonConverter, ISdkConfig config)
            : base(httpClientService, jsonConverter, config)
        {
        }

        public async Task<IResultList<ProductDto>> GetProducts()
        {
            return await GetAsync<ResultList<ProductDto>>(GetProductsUrl());
        }

        public async Task<ProductDto> GetProductById(int id)
        {
            return await GetAsync<ProductDto>(GetProductUrl(id));
        }

        public async Task<ProductDto> GetProductByName(string productName)
        {
            return await GetAsync<ProductDto>($"{GetProductsUrl()}/{productName}");
        }

        public async Task<IResultList<ProductVersionDto>> GetProductVersions(int productId)
        {
            return await GetAsync<ResultList<ProductVersionDto>>(GetVersionsUrl(productId));
        }

        public async Task<ProductVersionDto> GetProductVersionById(int productId, int versionId)
        {
            return await GetAsync<ProductVersionDto>(GetVersionUrl(productId, versionId));
        }

        public async Task<ProductVersionDto> GetProductVersionByVersion(int productId, string version)
        {
            return await GetAsync<ProductVersionDto>($"{GetVersionsUrl(productId)}/{version}");
        }

        public async Task<IResultList<ProductVersionProfileDto>> GetProductVersionProfiles(int productId, int versionId)
        {
            return await GetAsync<ResultList<ProductVersionProfileDto>>(GetProfilesUrl(productId, versionId));
        }

        public async Task<ProductVersionProfileDto> GetProductVersionProfile(
            int productId, 
            int versionId, 
            int profileId, 
            bool includeBranding = false)
        {
            var url = GetProfileUrl(productId, versionId, profileId);
            return includeBranding
                ? await GetAsync<ProductVersionProfileDto>($"{url}?includeBranding=true")
                : await GetAsync<ProductVersionProfileDto>(url);
        }

        public async Task<Stream> GetProductVersionProfileZip(int productId, int versionId, int profileId)
        {
            var url = $"{GetProfileUrl(productId, versionId, profileId)}/download";
            var response = await HttpClient.GetAsync(url);
            return await response.Content.ReadAsStreamAsync();
        }

        private string GetProductsUrl()
        {
            return $"{Config.BrandingApiEndpoint}/products";
        }

        private string GetProductUrl(int id)
        {
            return $"{GetProductsUrl()}/{id}";
        }

        private string GetVersionsUrl(int productId)
        {
            return $"{GetProductUrl(productId)}/versions";
        }

        private string GetVersionUrl(int productId, int versionId)
        {
            return $"{GetVersionsUrl(productId)}/{versionId}";
        }

        private string GetProfilesUrl(int productId, int versionId)
        {
            return $"{GetVersionUrl(productId, versionId)}/profiles";
        }

        private string GetProfileUrl(int productId, int versionId, int profileId)
        {
            return $"{GetProfilesUrl(productId, versionId)}/{profileId}";
        }
    }
}