﻿namespace Igor.Cloud.Platform.Sdk.Apis.Licensing.Applications
{
    public class ApplicationDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string CssIconClass { get; set; }
    }
}
