﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;

namespace Igor.Cloud.Platform.Sdk.Apis.Licensing.Applications
{
    public class ApplicationsService : ServiceBase, IApplicationsService
    {
        private const string ApplicationsRoot = "/applications";

        private readonly string _licensingEndpoint;

        public ApplicationsService(IHttpClientService httpClient, IJsonConverter jsonConverter, ISdkConfig config) : base(httpClient, jsonConverter, config)
        {
            _licensingEndpoint = $"{Config.LicensingApiEndpoint}{ApplicationsRoot}";
        }

        public async Task<IEnumerable<ApplicationDto>> GetAsync()
        {
            var licensingEndpoint = _licensingEndpoint;

            var applications = await GetAsync<List<ApplicationDto>>(licensingEndpoint);

            return applications;
        }
    }
}
