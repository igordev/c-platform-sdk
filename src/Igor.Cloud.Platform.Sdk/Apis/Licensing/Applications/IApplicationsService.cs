﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Core.Http;

namespace Igor.Cloud.Platform.Sdk.Apis.Licensing.Applications
{
    public interface IApplicationsService : IHasHttpClient
    {
        Task<IEnumerable<ApplicationDto>> GetAsync();
    }
}