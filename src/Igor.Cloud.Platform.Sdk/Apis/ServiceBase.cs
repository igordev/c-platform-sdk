﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Exceptions;

namespace Igor.Cloud.Platform.Sdk.Apis
{
    public abstract class ServiceBase
    {
        protected ServiceBase(
            IHttpClientService httpClient,
            IJsonConverter jsonConverter,
            ISdkConfig config)
        {
            HttpClient = httpClient;
            JsonConverter = jsonConverter;
            Config = config;
        }

        protected async Task<T> GetAsync<T>(string endpoint) where T : new()
        {
            var response = await HttpClient.GetAsync(endpoint);

            if (!response.IsSuccessStatusCode)
            {
                throw new IgorHttpException(response);
            }

            var responseMessage = await response.Content.ReadAsStringAsync();

            return !string.IsNullOrEmpty(responseMessage) 
                ? JsonConverter.Deserialize<T>(responseMessage) 
                : default(T);
        }

        protected async Task<HttpResponseMessage> Post(string endpoint, string body = null, ContentType contentType = ContentType.Json)
        {
            var content = GetContent(body, contentType);

            return await HttpClient.PostAsync(endpoint, content);
        }

        protected async Task<HttpResponseMessage> Put(string endpoint, string body = null, ContentType contentType = ContentType.Json)
        {
            var content = GetContent(body, contentType);

            return await HttpClient.PutAsync(endpoint, content);
        }

        protected async Task<HttpResponseMessage> Delete(string endpoint)
        {
            return await HttpClient.DeleteAsync(endpoint);
        }

        private static HttpContent GetContent(string body, ContentType contentType)
        {
            if (string.IsNullOrEmpty(body)) return null;

            var content = new StringContent(body);

            content.Headers.ContentType = contentType == ContentType.Xml 
                ? new MediaTypeHeaderValue("application/xml") 
                : new MediaTypeHeaderValue("application/json");

            return content;
        }

        public IHttpClientService HttpClient { get; }

        protected IJsonConverter JsonConverter { get; }

        protected ISdkConfig Config { get; }
    }
}
