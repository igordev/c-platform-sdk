﻿namespace Igor.Cloud.Platform.Sdk.Core.Config
{
    public class ApiSettings
    {
        public string AccountsUrl { get; set; } = "https://accounts-api-v1.igor-tech.com";
        public string LicensingUrl { get; set; } = "https://licensing-api-v1.igor-tech.com";
        public string BrandingUrl { get; set; } = "https://branding-api-v1.igor-tech.com";
    }
}
