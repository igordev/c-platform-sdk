﻿namespace Igor.Cloud.Platform.Sdk.Core.Config
{
    public interface ISdkConfig
    {
        string AccountsApiEndpoint { get; }
        string LicensingApiEndpoint { get; }
        string BrandingApiEndpoint { get; }
    }
}