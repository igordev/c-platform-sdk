﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace Igor.Cloud.Platform.Sdk.Core.Config
{
    public class SdkConfig : ISdkConfig
    {
        private readonly IConfiguration _config;
        private readonly HttpContext _httpContext;

        public SdkConfig(IConfiguration config, IHttpContextAccessor httpContextAccessor)
        {
            _config = config;
            _httpContext = httpContextAccessor.HttpContext;
        }

        public string AccountsApiEndpoint => TrimUrl(_config.GetValue<string>("ApiSettings:AccountsUrl"));

        public string LicensingApiEndpoint => DeterminePrefix(_config.GetValue<string>("ApiSettings:LicensingUrl"));

        public string BrandingApiEndpoint => TrimUrl(_config.GetValue<string>("ApiSettings:BrandingUrl"));

        private string DeterminePrefix(string url)
        {
            if (url.Contains("://"))
                return TrimUrl(url);

            var host = _httpContext.Request.Host.Value;

            if (host.Contains("localhost"))
                host = "localhost.igor-tech.com";
            
            var hostSuffix = host.Substring(host.IndexOf('.'));

            return $"https://{url}{hostSuffix}";
        }


        private static string TrimUrl(string url)
        {
            return url?.TrimEnd('/');
        }
    }
}
