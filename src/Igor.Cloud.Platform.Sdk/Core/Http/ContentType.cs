﻿namespace Igor.Cloud.Platform.Sdk.Core.Http
{
    public enum ContentType
    {
        Json,
        Xml
    }
}
