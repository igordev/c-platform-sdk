﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Igor.Cloud.Platform.Sdk.Core.Http
{
    public class HttpClientService : IHttpClientService
    {
        public HttpClient Client { get; }

        public HttpClientService()
        {
            Client = new HttpClient();
        }

        public void SetBearerToken(string token)
        {
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

        public async Task<string> GetStringAsync(string url)
        {
            return await Client.GetStringAsync(url);
        }

        public async Task<HttpResponseMessage> GetAsync(string url)
        {
            return await Client.GetAsync(url);
        }

        public async Task<HttpResponseMessage> PostAsync(string url, HttpContent content = null)
        {
            return await Client.PostAsync(url, content);
        }

        public async Task<HttpResponseMessage> PutAsync(string url, HttpContent content = null)
        {
            return await Client.PutAsync(url, content);
        }

        public async Task<HttpResponseMessage> DeleteAsync(string url)
        {
            return await Client.DeleteAsync(url);
        }
    }
}
