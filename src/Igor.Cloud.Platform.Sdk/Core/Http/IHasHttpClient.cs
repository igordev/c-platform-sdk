﻿namespace Igor.Cloud.Platform.Sdk.Core.Http
{
    public interface IHasHttpClient
    {
        IHttpClientService HttpClient { get; }
    }
}