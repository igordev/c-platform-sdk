﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Igor.Cloud.Platform.Sdk.Core.Http
{
    public interface IHttpClientService
    {
        HttpClient Client { get; }

        void SetBearerToken(string token);
        Task<string> GetStringAsync(string url);
        Task<HttpResponseMessage> GetAsync(string url);
        Task<HttpResponseMessage> PostAsync(string url, HttpContent content = null);
        Task<HttpResponseMessage> PutAsync(string url, HttpContent content = null);
        Task<HttpResponseMessage> DeleteAsync(string url);
    }
}