﻿using Newtonsoft.Json;

namespace Igor.Cloud.Platform.Sdk.Core.Serialization
{
    public interface IJsonConverter
    {
        string Serialize(object @object, Formatting formatting = Formatting.None);
        T Deserialize<T>(string @object);
    }
}