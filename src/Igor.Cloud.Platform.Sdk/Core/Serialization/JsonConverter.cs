﻿using Newtonsoft.Json;

namespace Igor.Cloud.Platform.Sdk.Core.Serialization
{
    public class JsonConverter : IJsonConverter
    {
        public string Serialize(object @object, Formatting formatting = Formatting.None)
        {
            return JsonConvert.SerializeObject(@object, formatting);
        }

        public T Deserialize<T>(string @object)
        {
            return JsonConvert.DeserializeObject<T>(@object);
        }
    }
}
