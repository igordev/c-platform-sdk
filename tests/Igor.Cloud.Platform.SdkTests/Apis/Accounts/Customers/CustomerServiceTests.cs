﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Customers;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Exceptions;
using Igor.Sdk.Common.Models;
using Moq;
using Xunit;

namespace Igor.Cloud.Platform.SdkTests.Apis.Accounts.Customers
{
    public class CustomerServiceTests
    {
        private readonly CustomerDto _customer;
        private readonly OperationDto _operation;
        private readonly string _customerJson;
        private readonly string _operationJson;
        private readonly ICustomerService _customerService;
        private readonly Mock<ISdkConfig> _sdkConfigMock;
        private readonly Mock<IHttpClientService> _httpClientServiceMock;
        private readonly Mock<IJsonConverter> _jsonConverterMock;
        private readonly Guid _customerId = Guid.NewGuid();
        private readonly Guid _userId = Guid.NewGuid();
        private readonly Guid _childCustomerId = Guid.NewGuid();
        private const int OperationId = 1;
        private const string AccountsApiEndpoint = "http://www.test.com";
        private const string CustomersRoot = "/customers";
        private const string OperationsRoot = "/operations";

        public CustomerServiceTests()
        {
            _customer = new CustomerDto {Id = _customerId, Name = "TestCustomer"};
            _operation = new OperationDto { Id = OperationId, OperationType = OperationType.None, ResourceId = _customerId, ResourceType = ResourceType.Customer };

            var jsonConverter = new JsonConverter();
            
            _customerJson = jsonConverter.Serialize(_customer);

            _operationJson = jsonConverter.Serialize(_operation);

            _httpClientServiceMock = new Mock<IHttpClientService>();
            _jsonConverterMock = new Mock<IJsonConverter>();

            _sdkConfigMock = new Mock<ISdkConfig>();
            _sdkConfigMock.Setup(p => p.AccountsApiEndpoint).Returns(AccountsApiEndpoint);

            _customerService = new CustomerService(_httpClientServiceMock.Object, _jsonConverterMock.Object, _sdkConfigMock.Object);
        }

        [Fact]
        public async Task TestShouldGetAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customerId}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) {Content = new StringContent($"[{_customerJson}]")};

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<CustomerDto>(It.IsAny<string>())).Returns(_customer);

            var customerService = new CustomerService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var customer = await customerService.GetAsync(_customerId);

            Assert.Equal(customer, _customer);
        }

        [Fact]
        public async Task TestGetAsyncShouldThrowException()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customerId}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.NotFound);

            var httpClientServiceMock = new Mock<IHttpClientService>();

            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var customerService = new CustomerService(httpClientServiceMock.Object, _jsonConverterMock.Object, _sdkConfigMock.Object);

            await Assert.ThrowsAsync<IgorHttpException>(async () => await customerService.GetAsync(_customerId));
        }

        [Fact]
        public async Task TestShouldGetListAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}?page=1&pageSize=20";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent($"[{_customerJson}]") };

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<PaginatedList<CustomerDto>>(It.IsAny<string>()))
                .Returns(new PaginatedList<CustomerDto>(new[] {_customer}, 1, 0, 50));

            var customerService = new CustomerService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var customers = await customerService.GetListAsync();

            Assert.Equal(customers.List, new List<CustomerDto>{_customer});
        }

        [Fact]
        public async Task TestShouldCreateAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}";

            var createCustomer = new CreateCustomerDto
            {
                Name = _customer.Name
            };
            await _customerService.CreateAsync(createCustomer);

            _httpClientServiceMock.Verify(p => p.PostAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public async Task TestShouldUpdateAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customerId}";

            var updateCustomer = new UpdateCustomerDto
            {
                Name = _customer.Name
            };

            await _customerService.UpdateAsync(_customer.Id, updateCustomer);

            _httpClientServiceMock.Verify(p => p.PutAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public async Task TestShouldDeleteAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customer.Id}";

            await _customerService.DeleteAsync(_customer.Id);

            _httpClientServiceMock.Verify(p => p.DeleteAsync(url), Times.Once);
        }

        [Fact]
        public async Task TestShouldAddUserAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customer.Id}/users/{_userId}";

            await _customerService.AddUserAsync(_customer.Id, _userId);

            _httpClientServiceMock.Verify(p => p.PostAsync(url, null), Times.Once);
        }

        [Fact]
        public async Task TestShouldRemoveUserAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customer.Id}/users/{_userId}";

            await _customerService.RemoveUserAsync(_customer.Id, _userId);

            _httpClientServiceMock.Verify(p => p.DeleteAsync(url), Times.Once);
        }

        [Fact]
        public async Task TestShouldAddChildCustomerAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customer.Id}{CustomersRoot}/{_childCustomerId}";

            await _customerService.AddChildCustomerAsync(_customer.Id, _childCustomerId);

            _httpClientServiceMock.Verify(p => p.PostAsync(url, null), Times.Once);
        }

        [Fact]
        public async Task TestShouldRemoveChildCustomerAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customer.Id}{CustomersRoot}/{_childCustomerId}";

            await _customerService.RemoveChildCustomerAsync(_customer.Id, _childCustomerId);

            _httpClientServiceMock.Verify(p => p.DeleteAsync(url), Times.Once);
        }

        [Fact]
        public async Task TestShouldGetOperationsForResourceAsync()
        {
            var url = $"{AccountsApiEndpoint}{CustomersRoot}/{_customer.Id}{OperationsRoot}?page=1&pageSize=20";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent($"[{_operationJson}]") };

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<PaginatedList<OperationDto>>(It.IsAny<string>())).Returns(new PaginatedList<OperationDto> { List = new[] { _operation } });

            var customerService = new CustomerService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var operations = await customerService.GetOperationsForResourceAsync(_customerId);

            Assert.Equal(new List<OperationDto> { _operation }, operations.List);
        }
    }
}
