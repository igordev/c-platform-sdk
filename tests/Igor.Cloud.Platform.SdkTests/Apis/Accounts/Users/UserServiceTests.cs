﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Security;
using Igor.Cloud.Platform.Sdk.Apis.Accounts.Users;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Exceptions;
using Igor.Sdk.Common.Models;
using Moq;
using Xunit;

namespace Igor.Cloud.Platform.SdkTests.Apis.Accounts.Users
{
    public class UserServiceTests
    {
        private readonly UserDto _user;
        private readonly OperationDto _operation;
        private readonly string _userJson;
        private readonly string _operationJson;
        private readonly IUserService _userService;
        private readonly Mock<ISdkConfig> _sdkConfigMock;
        private readonly Mock<IHttpClientService> _httpClientServiceMock;
        private readonly Mock<IJsonConverter> _jsonConverterMock;
        private readonly Guid _userId = Guid.NewGuid();
        private const int OperationId = 1;
        private const string AccountsApiEndpoint = "http://www.test.com";
        private const string UsersRoot = "/users";
        private const string OperationsRoot = "/operations";
        private const string RolesRoot = "/roles";
        
        public UserServiceTests()
        {
            _user = new UserDto { Id = _userId, FirstName = "Test", LastName = "User" };

            _operation = new OperationDto { Id = OperationId, OperationType = OperationType.None, ResourceId = _userId, ResourceType = ResourceType.User};

            var jsonConverter = new JsonConverter();
            
            _userJson = jsonConverter.Serialize(_user);

            _operationJson = jsonConverter.Serialize(_operation);


            _httpClientServiceMock = new Mock<IHttpClientService>();
            _jsonConverterMock = new Mock<IJsonConverter>();

            _sdkConfigMock = new Mock<ISdkConfig>();
            _sdkConfigMock.Setup(p => p.AccountsApiEndpoint).Returns(AccountsApiEndpoint);

            _userService = new UserService(_httpClientServiceMock.Object, _jsonConverterMock.Object, _sdkConfigMock.Object);
        }

        [Fact]
        public void TestHasHttpClientAvailable()
        {
            Assert.NotNull(_userService.HttpClient);
        }

        [Fact]
        public async Task TestShouldGetAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(_userJson) };

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<UserDto>(It.IsAny<string>())).Returns(_user);
            
            var userService = new UserService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var user = await userService.GetAsync(_userId);

            Assert.Equal(user, _user);
        }

        [Fact]
        public async Task TestGetAsyncShouldThrowException()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.NotFound);

            var httpClientServiceMock = new Mock<IHttpClientService>();

            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var userService = new UserService(httpClientServiceMock.Object, _jsonConverterMock.Object, _sdkConfigMock.Object);

            await Assert.ThrowsAsync<IgorHttpException>(async () => await userService.GetAsync(_userId));
        }

        [Fact]
        public async Task TestShouldGetListAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}?page=1&pageSize=20";
            
            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent($"[{_userJson}]") };

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<PaginatedList<UserDto>>(It.IsAny<string>()))
                .Returns(new PaginatedList<UserDto>(new[] {_user}, 1, 0, 50));
            
            var userService = new UserService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var users = await userService.GetListAsync();

            Assert.Equal(users.List, new List<UserDto>{_user});
        }

        [Fact]
        public void TestShouldCreateAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}";

            var createUser = new CreateUserDto
            {
                CustomerId = Guid.NewGuid(),
                Email = _user.Email,
                FirstName = _user.FirstName,
                IsActive = _user.IsActive,
                LastName = _user.LastName,
                Password = _user.Password,
                PhoneNumber = _user.PhoneNumber
            };

            _userService.CreateAsync(createUser);

            _httpClientServiceMock.Verify(p => p.PostAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public void TestShouldUpdateAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}";
            
            var updateUser = new UpdateUserDto
            {
                Email = _user.Email,
                FirstName = _user.FirstName,
                IsActive = _user.IsActive,
                LastName = _user.LastName,
                PhoneNumber = _user.PhoneNumber
            };

            _userService.UpdateAsync(_user.Id, updateUser);

            _httpClientServiceMock.Verify(p => p.PutAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public void TestShouldDeleteAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_user.Id}";

            _userService.DeleteAsync(_user.Id);

            _httpClientServiceMock.Verify(p => p.DeleteAsync(url), Times.Once);
        }

        [Fact]
        public async Task TestShouldGetOperationAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{OperationsRoot}/{OperationId}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(_operationJson) };
            
            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<OperationDto>(It.IsAny<string>())).Returns(_operation);

            var userService = new UserService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var operation = await userService.GetOperationAsync(_userId, OperationId);

            Assert.Equal(operation, _operation);
        }

        [Fact]
        public async Task TestGetOperationAsyncShouldThrowException()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{OperationsRoot}/{OperationId}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.NotFound);

            var httpClientServiceMock = new Mock<IHttpClientService>();

            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var userService = new UserService(httpClientServiceMock.Object, _jsonConverterMock.Object, _sdkConfigMock.Object);

            await Assert.ThrowsAsync<IgorHttpException>(async () => await userService.GetOperationAsync(_userId, OperationId));
        }

        [Fact]
        public async Task TestShouldGetOperationsForResourceAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{OperationsRoot}?page=1&pageSize=20";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent($"[{_operationJson}]") };

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<PaginatedList<OperationDto>>(It.IsAny<string>())).Returns(new PaginatedList<OperationDto> { List = new [] {_operation} });

            var userService = new UserService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);
            
            var operations = await userService.GetOperationsForResourceAsync(_userId);

            Assert.Equal(new List<OperationDto> { _operation }, operations.List);
        }

        [Fact]
        public void TestShouldCreateOperationAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{OperationsRoot}";

            var createOperation = new CreateOperationDto
            {
                OperationType = _operation.OperationType,
                ResourceId = _operation.ResourceId,
                ResourceType = _operation.ResourceType
            };

            _userService.CreateOperationAsync(_userId, createOperation);

            _httpClientServiceMock.Verify(p => p.PostAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public void TestShouldUpdateOperationAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{OperationsRoot}/{OperationId}";

            var updateOperation = new UpdateOperationDto
            {
                OperationType = _operation.OperationType,
                ResourceId = _operation.ResourceId,
                ResourceType = _operation.ResourceType
            };

            _userService.UpdateOperationAsync(_userId, _operation.Id, updateOperation);

            _httpClientServiceMock.Verify(p => p.PutAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public void TestShouldDeleteOperationAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{OperationsRoot}/{OperationId}";

            _userService.DeleteOperationAsync(_userId, OperationId);

            _httpClientServiceMock.Verify(p => p.DeleteAsync(url), Times.Once);
        }

        [Fact]
        public void TestShouldAddUserRoleAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{RolesRoot}";

            _userService.AddUserRoleAsync(_userId, RoleTypes.IgorAdmin);

            _httpClientServiceMock.Verify(p => p.PostAsync(url, It.IsAny<HttpContent>()), Times.Once);
        }

        [Fact]
        public void TestShouldRemoveUserRoleAsync()
        {
            var url = $"{AccountsApiEndpoint}{UsersRoot}/{_userId}{RolesRoot}/{RoleTypes.IgorAdmin}";

            _userService.RemoveUserRoleAsync(_userId, RoleTypes.IgorAdmin);

            _httpClientServiceMock.Verify(p => p.DeleteAsync(url), Times.Once);
        }
    }
}
