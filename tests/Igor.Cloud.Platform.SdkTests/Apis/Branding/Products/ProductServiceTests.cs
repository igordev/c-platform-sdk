﻿using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Branding.Models;
using Igor.Cloud.Platform.Sdk.Apis.Branding.Products;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Igor.Cloud.Platform.TestUtilities;
using Igor.Exceptions;
using Igor.Sdk.Common;
using Igor.Sdk.Common.Models;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace Igor.Cloud.Platform.SdkTests.Apis.Branding.Products
{
    public class ProductServiceTests
    {
        private const string BrandingUrl = "https://got-branding.com";
        private readonly Mock<IHttpClientService> _mockHttpClientService;
        private readonly ProductService _service;

        public ProductServiceTests()
        {
            var jsonConverter = new JsonConverter();
            
            var config = SdkConfigUtility.GetConfiguration(new ApiSettings
            {
                BrandingUrl = BrandingUrl
            });

            var sdkConfig = new SdkConfig(config, new Mock<IHttpContextAccessor>().Object);

            _mockHttpClientService = new Mock<IHttpClientService>();
            _service = new ProductService(_mockHttpClientService.Object, jsonConverter, sdkConfig);
        }

        [Fact]
        public async Task ShouldReturnAllProducts()
        {
            var expected =
                new ResultList<ProductDto> {List = new[] {new ProductDto(), new ProductDto(), new ProductDto()}};

            _mockHttpClientService.SetupGetJsonResponse($"{BrandingUrl}/products", expected);

            var actual = await _service.GetProducts();
            Assert.Equal(3, actual.List.Count());
        }

        [Fact]
        public async Task GetProductsShouldThrowIgorHttpException()
        {
            _mockHttpClientService.SetupGetNotFoundResponse();

            await Assert.ThrowsAsync<IgorHttpException>(() => _service.GetProducts());
        }

        [Fact]
        public async Task ShouldReturnProductForId()
        {
            var expected = new ProductDto {Id = 15, Name = "jack"};

            _mockHttpClientService.SetupGetJsonResponse($"{BrandingUrl}/products/15", expected);

            var actual = await _service.GetProductById(15);
            Assert.Equal("jack", actual.Name);
            Assert.Equal(15, actual.Id);
        }

        [Fact]
        public async Task ShouldReturnProductForName()
        {
            var expected = new ProductDto { Id = 15, Name = "jack" };

            _mockHttpClientService.SetupGetJsonResponse($"{BrandingUrl}/products/jack", expected);

            var actual = await _service.GetProductByName("jack");
            Assert.Equal("jack", actual.Name);
            Assert.Equal(15, actual.Id);
        }

        [Fact]
        public async Task GetProductByIdShouldThrowIgorHttpException()
        {
            _mockHttpClientService.SetupGetNotFoundResponse();

            await Assert.ThrowsAsync<IgorHttpException>(() => _service.GetProductById(34));
        }

        [Fact]
        public async Task ShouldGetProductVersions()
        {
            var expected = new[] {new ProductVersionDto(), new ProductVersionDto(), new ProductVersionDto()}
                .ToResultList();

            _mockHttpClientService.SetupGetJsonResponse($"{BrandingUrl}/products/54/versions", expected);

            var actual = await _service.GetProductVersions(54);
            Assert.Equal(3, actual.TotalCount);
        }

        [Fact]
        public async Task ShouldThrowHttpExceptionGetProductVersions()
        {
            _mockHttpClientService.SetupGetNotFoundResponse();

            await Assert.ThrowsAsync<IgorHttpException>(() => _service.GetProductVersions(34));
        }

        [Fact]
        public async Task ShouldGetProductVersionForId()
        {
            var expected = new ProductVersionDto {Id = 34};
            _mockHttpClientService.SetupGetJsonResponse($"{BrandingUrl}/products/45/versions/34", expected);

            var actual = await _service.GetProductVersionById(45, 34);
            Assert.Equal(34, actual.Id);
        }

        [Fact]
        public async Task ShouldGetProductVersionForVersion()
        {
            var expected = new ProductVersionDto { Id = 34, StartVersion = "3.0.0", EndVersion = "4.0.0"};
            _mockHttpClientService.SetupGetJsonResponse($"{BrandingUrl}/products/45/versions/3.2.7", expected);

            var actual = await _service.GetProductVersionByVersion(45, "3.2.7");
            Assert.Equal(34, actual.Id);
        }

        [Fact]
        public async Task ShouldThrowHttpExceptionForGetProductVersion()
        {
            _mockHttpClientService.SetupGetNotFoundResponse();

            await Assert.ThrowsAsync<IgorHttpException>(() => _service.GetProductVersionById(34, 12));
        }

        [Fact]
        public async Task ShouldGetProductVersionProfiles()
        {
            var expected = new[] {new ProductVersionProfileDto(), new ProductVersionProfileDto()}.ToResultList();

            _mockHttpClientService
                .SetupGetJsonResponse($"{BrandingUrl}/products/5/versions/98/profiles", expected);

            var actual = await _service.GetProductVersionProfiles(5, 98);
            Assert.Equal(2, actual.List.Count);
        }

        [Fact]
        public async Task ShouldThrowHttpExceptionForGetProductVersionProfiles()
        {
            _mockHttpClientService.SetupGetNotFoundResponse();

            await Assert.ThrowsAsync<IgorHttpException>(() => _service.GetProductVersionProfiles(3, 2));
        }

        [Fact]
        public async Task ShouldGetProductVersionProfile()
        {
            var expected = new ProductVersionProfileDto { Id = 45, ProductId = 3, VersionId = 54};

            _mockHttpClientService
                .SetupGetJsonResponse($"{BrandingUrl}/products/3/versions/54/profiles/45", expected);

            var actual = await _service.GetProductVersionProfile(3, 54, 45);
            Assert.Equal(45, actual.Id);
            Assert.Equal(3, actual.ProductId);
            Assert.Equal(54, actual.VersionId);
        }
        
        [Fact]
        public async Task ShouldThrowHttpExceptionForGetProductVersionProfile()
        {
            _mockHttpClientService.SetupGetNotFoundResponse();
            
            await Assert.ThrowsAsync<IgorHttpException>(() => _service.GetProductVersionProfile(7, 3, 1));
        }

        [Fact]
        public async Task ShouldGetProductVersionProfileWithBrandingIncluded()
        {
            var expected = new ProductVersionProfileDto {Branding = new BrandingDto {StyleTechnology = "idk"}};
            _mockHttpClientService
                .SetupGetJsonResponse($"{BrandingUrl}/products/5/versions/1/profiles/9?includeBranding=true", expected);

            var actual = await _service.GetProductVersionProfile(5, 1, 9, true);
            Assert.Equal("idk", actual.Branding.StyleTechnology);
        }

        [Fact]
        public async Task ShouldProductVersionProfileOutput()
        {
            var bytes = new MemoryStream(Encoding.UTF8.GetBytes("one"));
            _mockHttpClientService
                .Setup(s => s.GetAsync($"{BrandingUrl}/products/54/versions/12/profiles/65/download"))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(bytes)
                });

            var stream = await _service.GetProductVersionProfileZip(54, 12, 65);
            var actual = new StreamReader(stream).ReadToEnd();
            Assert.Equal("one", actual);
        }
    }
}