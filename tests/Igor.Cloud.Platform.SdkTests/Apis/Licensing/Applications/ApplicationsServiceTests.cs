﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Igor.Cloud.Platform.Sdk.Apis.Licensing.Applications;
using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Moq;
using Xunit;

namespace Igor.Cloud.Platform.SdkTests.Apis.Licensing.Applications
{
    public class ApplicationsServiceTests
    {
        private readonly ApplicationDto _application;
        private readonly string _applicationJson;
        private readonly Mock<ISdkConfig> _sdkConfigMock;
        private readonly ApplicationsService _applicationsService;
        private const string LicensingApiEndpoint = "http://www.test.com";
        private const string ApplicationsRoot = "/applications";
        
        public ApplicationsServiceTests()
        {
            _application = new ApplicationDto { Name = "Licensing", Url = "https://www.test.com" };
            
            var jsonConverter = new JsonConverter();
            
            _applicationJson = jsonConverter.Serialize(new List<ApplicationDto> { _application });

            var httpClientServiceMock = new Mock<IHttpClientService>();
            var jsonConverterMock = new Mock<IJsonConverter>();

            _sdkConfigMock = new Mock<ISdkConfig>();
            _sdkConfigMock.Setup(p => p.LicensingApiEndpoint).Returns(LicensingApiEndpoint);

            _applicationsService = new ApplicationsService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);
        }

        [Fact]
        public void TestHasHttpClientAvailable()
        {
            Assert.NotNull(_applicationsService.HttpClient);
        }

        [Fact]
        public async Task TestShouldGetAsync()
        {
            var url = $"{LicensingApiEndpoint}{ApplicationsRoot}";

            var expectedResponse =
                new HttpResponseMessage(HttpStatusCode.OK) { Content = new StringContent(_applicationJson) };

            var httpClientServiceMock = new Mock<IHttpClientService>();
            httpClientServiceMock.Setup(p => p.GetAsync(url)).ReturnsAsync(expectedResponse);

            var jsonConverterMock = new Mock<IJsonConverter>();
            jsonConverterMock.Setup(p => p.Deserialize<List<ApplicationDto>>(It.IsAny<string>())).Returns(new List<ApplicationDto>{_application});
            
            var applicationsService = new ApplicationsService(httpClientServiceMock.Object, jsonConverterMock.Object, _sdkConfigMock.Object);

            var applications = await applicationsService.GetAsync();

            Assert.Equal(applications, new List<ApplicationDto>{_application});
        }
    }
}
