﻿using Igor.Cloud.Platform.Sdk.Core.Config;
using Igor.Cloud.Platform.TestUtilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace Igor.Cloud.Platform.SdkTests.Core.Config
{
    public class SdkConfigTests
    {
        private readonly IConfiguration _config;
        private readonly SdkConfig _sdkConfig;

        public SdkConfigTests()
        {
            var context = new DefaultHttpContext();
            context.Request.Host = new HostString("https://something.host.com");

            var mockAccessor = new Mock<IHttpContextAccessor>();
            mockAccessor.SetupGet(p => p.HttpContext)
                .Returns(context);

            _config = SdkConfigUtility.GetConfiguration();
            _sdkConfig = new SdkConfig(_config, mockAccessor.Object);
        }

        [Fact]
        public void TestAccountsApiEndpointShouldReturnDefault()
        {
            const string expected = "https://accounts-api-v1.igor-tech.com";
            Assert.Equal(expected, _sdkConfig.AccountsApiEndpoint);
        }

        [Fact]
        public void TestAccountsApiEndpointShouldReturnConfig()
        {
            const string testConfig = "http://test.sdkConfig.com";
            _config["ApiSettings:AccountsUrl"] = testConfig;

            Assert.Equal(testConfig, _sdkConfig.AccountsApiEndpoint);
        }

        [Fact]
        public void TestAccountsApiEndpointShouldReturnTrimTrailingSlash()
        {
            const string testConfig = "http://test.sdkConfig.com/";
            _config["ApiSettings:AccountsUrl"] = testConfig;

            const string expected = "http://test.sdkConfig.com";
            
            Assert.Equal(expected, _sdkConfig.AccountsApiEndpoint);
        }

        [Fact]
        public void BrandingApiEndpointShouldReturnDefaultEndpoint()
        {
            Assert.Equal("https://branding-api-v1.igor-tech.com", _sdkConfig.BrandingApiEndpoint);
        }

        [Fact]
        public void LicensingApiEndpointShouldReturnDefaultEndpoint()
        {
            Assert.Equal("https://licensing-api-v1.igor-tech.com", _sdkConfig.LicensingApiEndpoint);
        }

        [Fact]
        public void LicensingApiEndpointShouldUsePrefix()
        {
            const string testConfig = "licensing-test";
            _config["ApiSettings:LicensingUrl"] = testConfig;

            var expected = $"https://{testConfig}.host.com";
            
            Assert.Equal(expected, _sdkConfig.LicensingApiEndpoint);
        }
    }
}