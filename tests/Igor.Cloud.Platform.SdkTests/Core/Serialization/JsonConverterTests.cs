﻿using System;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Xunit;

namespace Igor.Cloud.Platform.SdkTests.Core.Serialization
{
    public class JsonConverterTests
    {
        [Fact]
        public void TestShouldSerializeObject()
        {
            var testObject = new TestObject("test1", "test2");
            const string expected = "{\"Value1\":\"test1\",\"Value2\":\"test2\"}";

            var converter = new JsonConverter();

            Assert.Equal(converter.Serialize(testObject), expected);
        }

        [Fact]
        public void TestShouldDeserializeObject()
        {
            const string testJson = "{\"Value1\":\"test1\",\"Value2\":\"test2\"}";
            var expected = new TestObject("test1", "test2");

            var converter = new JsonConverter();

            Assert.Equal(converter.Deserialize<TestObject>(testJson), expected);
        }
    }

    public class TestObject
    {
        public TestObject(string value1, string value2)
        {
            Value1 = value1;
            Value2 = value2;
        }

        public string Value1 { get; set; }
        public string Value2 { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as TestObject;
            return item != null && Equals(item);
        }

        protected bool Equals(TestObject other)
        {
            return string.Equals(Value1, other.Value1) && string.Equals(Value2, other.Value2);
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }
}