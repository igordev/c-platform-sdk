﻿using System.Net;
using System.Net.Http;
using Igor.Cloud.Platform.Sdk.Core.Http;
using Igor.Cloud.Platform.Sdk.Core.Serialization;
using Moq;

namespace Igor.Cloud.Platform.SdkTests
{
    public static class MockHttpClientServiceExtensions
    {
        public static void SetupGetResponse(this Mock<IHttpClientService> mock, string url, string content)
        {
            mock.Setup(s => s.GetAsync(url))
                .ReturnsAsync(CreateResponseFromString(content));
        }

        public static void SetupGetJsonResponse<T>(this Mock<IHttpClientService> mock, string url, T data)
        {
            var json = new JsonConverter().Serialize(data);
            mock.SetupGetResponse(url, json);
        }

        public static void SetupGetNotFoundResponse(this Mock<IHttpClientService> mock)
        {
            mock.Setup(s => s.GetAsync(It.IsAny<string>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.NotFound));
        }

        private static HttpResponseMessage CreateResponseFromString(string content)
        {
            return new HttpResponseMessage
            {
                Content = new StringContent(content)
            };
        }
    }
}